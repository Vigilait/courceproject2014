package lexerparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lexerparser.OutputClass;

public class Lexer {
	public enum TokenType {
		
		SHOW("Show"),
		CREATE("Create"),
		BEGIN("Begin"),
		END("End"),
		EQUALS("="),
		STRINGEND(";"),
		MATHOP("\\*|\\+|-|/"),
		OPENSK("\\("),
		CLOSESK("\\)"),
		NUMBER("[0-9]+"),
		VARIABLE("[a-z]+"),
		WHITESPACE("/s");
		

        public String pattern;

        TokenType(String p) {
            pattern = p;
        }

    }
	
    public static String readFile (String fileName) {
        try {
            StringBuffer out = new StringBuffer();
            FileReader f = new FileReader(fileName);
            BufferedReader b = new BufferedReader(f);

            String s;
            while((s = b.readLine()) != null) {
                out.append(s);
            }

            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
	
	public static ArrayList<Token> getList(String in) {
        ArrayList<Token> tokenList = new ArrayList<Token>();

        StringBuffer tokenBuffer = new StringBuffer();
        for(TokenType t : TokenType.values()) {

            tokenBuffer.append("(?<" + t.name() + ">" + t.pattern + ")|");
        }

        Pattern pattern = Pattern.compile(tokenBuffer.toString());
        Matcher matcher = pattern.matcher(readFile(in));
        int num = 0;
        OutputClass.outDbg("Token List: \n");
        while(matcher.find()) {
            for(TokenType t : TokenType.values()) {

                if (matcher.group(t.name()) != null) {
                    if (t.name() == "WHITESPACE") {
                        continue;
                    }

                    tokenList.add(new Token(t, matcher.group(t.name())));
                	num = num + 1;
                    OutputClass.outDbg(num + " "+ t + "  " + matcher.group(t.name()));
                }
            }
        }

        return tokenList;
    }

}
