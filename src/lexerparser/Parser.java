package lexerparser;

import java.util.ArrayList;
import java.util.ListIterator;

import lexerparser.Inter;

public class Parser {
    ListIterator<Token> list;
    boolean expFound;

    Parser(ArrayList<Token> newList) {
        list = newList.listIterator();
        expFound = false;
    }

    String nextToken() {
        if (!list.hasNext())
               return "EOF";

        return list.next().tokenType.name();
    }

    boolean exp() {
        if (!expFound) {
            expFound = true;
            list.previous();
            OutputClass.outStd("ERROR: " + "("+ (list.nextIndex()+1) +")" + nextToken());
        }

        return false;
    }

    boolean checkFunction() {
       	if (nextToken().equals("VARIABLE"))
       		if (nextToken().equals("STRINGEND"))
       			return true;
       		else return exp();
        return exp();
    }
    boolean checkMaths() {
    	if (nextToken().equals("EQUALS")){
    		String token = nextToken();
        	while(!token.equals("STRINGEND")){	
	        	if(token.equals("OPENSK"))
	        		if(!checkMathsSK()) 
	        			return false;
	        		else {
	        			token = nextToken();	
	        			if(token.equals("MATHOP")){
	        				token = nextToken();
	        				continue;
	        			}
	        			continue;
	        		}
	        	if(token.equals("VARIABLE") || token.equals("NUMBER")){
	        		token = nextToken();
	        		if(token.equals("STRINGEND"))
	        			return true;
	        		if(token.equals("MATHOP")){
	        			if(nextToken().equals("STRINGEND")){
	        				return exp();
	        			} else {
	        				list.previous();
	        			}
	        			token = nextToken();
	        			continue;
	        		}
	        		else 
	        			return exp();
	        	} else
	        		return exp();	
	        	//token = nextToken();
        	}
        	if(token.equals("STRINGEND")) return true;
    	} else
    		return exp();
        return true;
    }
    boolean checkMathsSK() {
    	String token = nextToken();
    	if(token.equals("VARIABLE") || token.equals("NUMBER"))
    		while(!token.equals("CLOSESK")){
    			token = nextToken();
    			if(token.equals("OPENSK")){
    				if(!checkMathsSK()) 
	        			return false;
    			}
    			if(token.equals("MATHOP")){
    				token = nextToken();
    				if(token.equals("OPENSK")){
        				if(!checkMathsSK()) 
    	        			return false;
        				else continue;
        			}
        			if(token.equals("VARIABLE") || token.equals("NUMBER")){
        				continue;
    					}
    				else
    					{
    					
    						return exp();
    					}
    			} else 
    				if(token.equals("CLOSESK")) return true;
    				else {
    					return exp();
    			}
    		}
    	if(token.equals("CLOSESK")) return true;
    	else 
    		return exp();

    }




    boolean body() {
        if (nextToken().equals("BEGIN")){
            String token = nextToken();
            while (!token.equals("END") && !token.equals("EOF")) {
                if(token.equals("CREATE") || token.equals("SHOW"))
                	if(!checkFunction()) return false;
                	else {
                		token = nextToken();
                		continue;
                	}
                if(token.equals("VARIABLE"))
                	if(!checkMaths()) return false;
                	else {
                		token = nextToken();
                		continue;
                	}
            }
            if(token.equals("EOF")){
            	list.previous();
            	//list.previous();
            	if(!nextToken().equals("END")){
            		OutputClass.outStd("End NOT FOUND");
            		return false;
            	}
            }
        }
        return true;
    }

    public void parse(String fileInput) {
        if (body()){
            OutputClass.outDbg("\nSyntax is correct");
            Inter inter = new Inter();
            inter.start(fileInput);
        }
        
    }
}