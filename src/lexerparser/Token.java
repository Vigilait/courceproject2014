package lexerparser;

import lexerparser.Lexer.TokenType;



public class Token {
    public TokenType tokenType;
    public String value;

    Token(TokenType t, String v) {
        tokenType = t;
        value = v;
    }


    public String toString() {
        return ("<" + tokenType.name() + "> " + value);
    }
}
