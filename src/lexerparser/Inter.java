package lexerparser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import lexerparser.OutputClass;

public class Inter {
	
	Map<String, Double> myMap = new HashMap<String, Double>();
	Map<String, Double> vars = (Map<String, Double>)myMap;
	ArrayList<String> names = new ArrayList<String>();
	ArrayList<String> output = new ArrayList<String>();

	boolean start(String fileName){
		OutputClass.outDbg("\nCompilation start");
		String[] doc;
		doc = OutputClass.readFile(fileName).split(";");
		doc[0] = doc[0].replace("Begin","");
		doc[doc.length-1] = doc[doc.length-1].replace("End","");
		for(int i = 0; i < doc.length; i++){
			String current = doc[i];
			if(current.length() < 1) continue;
			OutputClass.outDbg("#" + i + " " + doc[i]);
			if(current.contains("Create")) funCreate(current);
				else if(current.contains("Show")) funShow(current);
					else funMaths(current);
				
		}
		OutputClass.outDbg("Compilation complete");
		return true;
	}
	
	void funCreate(String current){
		current = current.replace("Create","");
		current = current.trim();
		if(names.contains(current)) return;
		
		vars.put(current,0.0);
		names.add(current);
		OutputClass.outDbg("> Var " + current + " created");	
	}
	void funShow(String current){
		current = current.replace("Show","");
		current = current.trim();
		output.add("> Var " + current + " = " + vars.get(current));
		OutputClass.outStd("> Var " + current + " = " + vars.get(current));
	}
	void funMaths(String current){
		int eqPos = current.indexOf("=");
		String varName = String.copyValueOf(current.toCharArray(),0,eqPos);
		String workStr = String.copyValueOf(current.toCharArray(),eqPos + 1,current.length() - eqPos - 1);
		varName = varName.trim();
		workStr = workStr.trim();
		double result = 0;
		
		for(int i = 0; i < names.size(); i++){
			for(int k = 0; k < names.size() - i - 1; k ++){
				if(names.get(k).length() < names.get(k + 1).length()){
					String a = names.get(k);
					names.set(k, names.get(k + 1));
					names.set(k+1 , a);
				}
			}
		}
		for(int i = 0; i < names.size(); i++){
			workStr = workStr.replace(names.get(i), vars.get(names.get(i)).toString());
		}
		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		try {
			result = (double) engine.eval(workStr);
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			OutputClass.outStd("Error via calculating: no such VARIABLE or MATHS FUNCTION");
		}
		vars.put(varName, result);
	}	
}
