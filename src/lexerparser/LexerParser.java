package lexerparser;

import lexerparser.Token;
import lexerparser.Lexer;
import lexerparser.Parser;
import java.util.ArrayList;

import lexerparser.OutputClass;

public class LexerParser {
    public static void main (String a[]) {
    	OutputClass.init();
    	String fileInput = "input.txt";    	
        ArrayList<Token> tokens = Lexer.getList(fileInput);
        Parser parser = new Parser(tokens);
        parser.parse(fileInput);
        OutputClass.saveList(tokens);
    }
}

