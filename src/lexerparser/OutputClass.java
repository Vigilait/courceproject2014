package lexerparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OutputClass {

	static String fileOut = "output.txt";
	static String fileOutDBG = "output_DEBUG.txt";
	public static void init(){
		String string = "";
			try {
			
			FileWriter f = new FileWriter(fileOut);
			string += System.getProperty("line.separator");

			int size = string.length();
			char[] msg_d = new char[size];
			string.getChars(0,size,msg_d,0);

			for(int i=0; i<size; i++)
				f.write(msg_d[i]);

			f.close(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			FileWriter f = new FileWriter(fileOutDBG);
			string += System.getProperty("line.separator");

			int size = string.length();
			char[] msg_d = new char[size];
			string.getChars(0,size,msg_d,0);

			for(int i=0; i<size; i++)
				f.write(msg_d[i]);

			f.close(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void outStd(String string){
		saveFile(fileOut, string);
		outDbg(string);
	}
	public static void outDbg(String string){
		Calendar cal = Calendar.getInstance();
    	cal.getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    	String time =  sdf.format(cal.getTime());
    	string = time + " " + string;
		saveFile(fileOutDBG, string);
		System.out.println(string);
	}
	public static void saveList (ArrayList<Token> tokens) {		
		StringBuilder stringBuilder = new StringBuilder();
		for(Token t : tokens) {
			stringBuilder.append(t.toString() + "\n");
        }
	}
	public static void saveFile (String fileName, String string) {
		try {
			FileWriter f = new FileWriter(fileName, true);
			string += System.getProperty("line.separator");

			int size = string.length();
			char[] msg_d = new char[size];
			string.getChars(0,size,msg_d,0);

			for(int i=0; i<size; i++)
				f.write(msg_d[i]);

			f.close(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static String readFile (String fileName) {
        try {
            StringBuffer out = new StringBuffer();
            FileReader f = new FileReader(fileName);
            BufferedReader b = new BufferedReader(f);
            String s;
            while((s = b.readLine()) != null) {
                out.append(s);
            }
            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
	
	
}
