# This is my README
 Main class - LexerParser (the anme was just got from old project)
 **Language syntax:**
 	-Create var1; - creates a double variable with name "var1"
 	-Show var2;  - show the number of var2 in double type
 	-a = <maths>; -  mathematical calculations, u can use {"+","-","/","*","(",")"}
 **Eg:**
 	Begin
 	Create a;
 	Create b;
 	Create c;
 	Create d;
 	a = 5 + 7 + 6;
 	a = b + 74;
 	a = b + 2 * (a + b) + 2 * (7 + 4);
 	b = a / 2;
 	c = 5 + (6 + (5 + 4)) + a;
 	d = 2;
 	d = d * d * d * d * d * d * d;
 	Show a;
 	Show b;
 	Show c;
 	Show d;
 	End;
 
** Token List:**
 	- SHOW("Show")
 	- CREATE("Create")
 	- BEGIN("Begin")
 	- END("End")
 	- EQUALS("=")
 	- STRINGEND(";")
 	- MATHOP("\\*|\\+|-|/")
 	- OPENSK("\\(")
 	- CLOSESK("\\)")
 	- NUMBER("[0-9]+")
 	- VARIABLE("[a-z]+")
 	- WHITESPACE("[ \t\n\f\r]")
 
** Parser Grammar:**
 	- body: BEGIN function|maths END
 	- function: CREATE|SHOW VARIABLE STRINGEND
 	- maths: VARIABLE EQUALS opensk|VARIABLE|NUMBER MATHOP opensk|VARIABLE|NUMBER STRINGEND
 	- opensk: opensk|VARIABLE|NUMBER MATHOP opensk|VARIABLE|NUMBER CLOSESK
